export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v10045`, ...msg);
		};

		// script details
		const id = 'semi-auto-smith-bars';
		const title = 'SEMI Auto Smith Bars';

		// setting groups
		const SETTING_GENERAL = ctx.settings.section('General');
		const SETTING_BARS = ctx.settings.section('Bar Smithing');

		// variables
		const smithCategories = game.smithing.categories.allObjects;
		const barActions = game.smithing.actions.filter(x => x.category.id == 'melvorD:Bars' || x.category.id == 'melvorItA:AbyssalBars');
		const barPriority = barActions.sort((a, b) => (b.level + b.abyssalLevel + smithCategories.indexOf(b.category) * 1000) - (a.level + a.abyssalLevel + smithCategories.indexOf(a.category) * 1000));
		const barCount = barPriority.length;

		const bankHasRoom = (item) => game.bank.getQty(item) > 0 || game.bank.occupiedSlots < game.bank.maximumSlots;
		const canAffordAction = (action) => game.smithing.getRecipeCosts(action).checkIfOwned();
		const actionIsValid = (skill, action) => {
			return skill.level >= action.level && skill.abyssalLevel >= action.abyssalLevel && action.realm.isUnlocked;
		};

		const htmlID = (id) => id.replace(/[:_]/g, "-").toLowerCase();

		/**
		 * Find Next Valid Bar
		 * Basically checks every recipe going down from the current one.
		 * If none are possible to smith, then ends smithing without causing the script
		 * to go through each recipe over and over doing nothing.
		 */
		const findValidBar = (startIndex) => {
			let nextBarAction;
			let nextBar = startIndex;

			for (let i = 0; i < barCount; i++) {
				nextBar = (nextBar + 1) % barCount;
				nextBarAction = barPriority[nextBar];

				if (actionIsValid(game.smithing, nextBarAction) // Can smith
						&& SETTING_BARS.get(`smith-${htmlID(nextBarAction.id)}`) // Bar Enabled
						&& bankHasRoom(nextBarAction.product) // Has Room for product
						&& canAffordAction(nextBarAction)) { // Can afford to craft
					debugLog(`${startIndex} -> ${nextBar}`);
					return nextBarAction;
				}
			}

			return null;
		}

		/**
		* Called after `game.smithing.stop()`
		*/
		const onSmithingStop = () => {
			if (!SETTING_GENERAL.get(`${id}-enable`)) {
				return;
			}

			const currentBar = barPriority.findIndex(x => x === game.smithing.selectedRecipe)

			// If the current recipe isn't a bar, we don't wanna swap to bars.
			if (currentBar < 0) {
				return;
			}
			else {
				// If we still have enough to smith, then it was stopped for other reasons.
				if (game.smithing.getCurrentRecipeCosts().checkIfOwned()) {
					debugLog(`Smithing ended for outside reason.`);
					return;
				}

				// Find next Bar
				const nextBarAction = findValidBar(currentBar);

				// Have Valid Bar
				if (nextBarAction) {
					game.smithing.selectRecipeOnClick(nextBarAction);
					debugLog(`Switched to smithing ${nextBarAction.name}`);
					game.smithing.start();
				}
				// No more bars to smith, stop.
				else {
					debugLog(`No more applicable bars smithable.`);
					return;
				}
			}
		};

		// settings
		SETTING_GENERAL.add([{
			'type': 'switch',
			'name': `${id}-enable`,
			'label': `Enable ${title}`,
			'hint': 'Begin by smithing any type of bar, Auto Smith Bars will cycle through bars from highest to lowest until no more can be smithed.',
			'default': true
		}]);

		SETTING_BARS.add(barPriority.map(action => {
			return {
				"type": "switch",
				"name": `smith-${htmlID(action.id)}`,
				"label": $(`<span><img src="${action.realm.media}" class="resize-24"> <img src="${action.media}" class="resize-24"> Smith ${action.name}</span>`).get(0),
				"default": true
			}
		}));

		// hooks + game patches
		ctx.onCharacterLoaded((ctx) => {
			ctx.patch(Smithing, 'stop').after(() => {
				onSmithingStop();
			});
		});
	});
}
